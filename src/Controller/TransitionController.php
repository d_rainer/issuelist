<?php

namespace IssueList\Controller;

use IssueList\Controller\Controller,
    IssueList\Models\Transition;

class TransitionController extends Controller {

    function __construct() {
        parent::__construct();
    }

    public function getTransitions(): array {
        $query = 'SELECT * FROM `state_transitions`';
        return $this->getDataForModel($query, Transition::class);
    }

}
