<?php

namespace IssueList\Controller;

use IssueList\Controller\Controller,
    IssueList\Models\State;

class StateController extends Controller {

    public function __construct() {
        parent::__construct();
    }

    public function getStates($orderby = null, $orientation = null): array {
        $query = ($orderby !== null && $orientation !== null && in_array($orientation, ['ASC', 'DESC'])) ? 'SELECT * FROM `states` ORDER by ' . $orderby . ' ' . $orientation : 'SELECT * FROM `states`';
        return $this->getDataForModel($query, State::class);
    }

    public function getStatesByPriority(): array {
        return $this->getStates('priority', 'ASC');
    }

}
