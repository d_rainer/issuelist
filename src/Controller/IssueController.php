<?php

namespace IssueList\Controller;

use IssueList\Controller\Controller,
    IssueList\Models\Issue,
    IssueList\Models\State;

class IssueController extends Controller {

    function __construct() {
        parent::__construct();
    }

    public function getIssues(State $state = null): array {
        $query = ($state !== null) ? "SELECT * FROM `issues` WHERE `state` = '" . $state->id . "'" : 'SELECT * FROM `issues`';
        return $this->getDataForModel($query, Issue::class);
    }

    public function getIssuesByState(State $state): array {
        return $this->getIssues($state);
    }

}
