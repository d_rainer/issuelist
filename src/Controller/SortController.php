<?php

namespace IssueList\Controller;

use IssueList\Controller\IssueController,
    IssueList\Controller\StateController;

class SortController extends Controller {

    public function __construct() {
        parent::__construct();

        $this->stateController = new StateController();
        $this->issueController = new IssueController();

        if ($this->install === TRUE) {
            $this->install();
        }
    }

    private function install() {
        $installQuery = file_get_contents('sql/install.sql');
        try {
            $this->dbh->query($installQuery);
        } catch (\PDOException $e) {
            echo 'cannot install database infrastructure';
            exit;
        }
    }

    public function addIssuesAndListIssues() {
        $query = file_get_contents('sql/addissues.sql');
        $this->dbh->query($query);

        $this->listIssuesByState();
    }

    public function addNewStateAndListIssues() {
        $this->dbh->query("INSERT INTO `states` (`name`, `priority`) VALUES ('Try again', 6)");
        $newStateId = $this->dbh->lastInsertId();

        $this->dbh->query("INSERT INTO `state_transitions` (`from_state`, `to_state`) VALUES (5," . $newStateId . ")");
        $this->dbh->query("UPDATE `issues` SET `state` = " . $newStateId . " WHERE `title` = 'Repair old coffee machine'");

        $this->listIssuesByState();
    }

    public function listIssuesByState() {
        $states = $this->stateController->getStatesByPriority();

        foreach ($states as $state) {
            print $state->name . "\n";

            $issues = $this->issueController->getIssuesByState($state);
            foreach ($issues as $issue) {
                print '  ' . $issue->name . "\n";
            }

            print "\n";
        }
    }

}
