<?php

namespace IssueList\Controller;

use IssueList\Models\Modelinterface;

require('settings.php');

abstract class Controller {

    protected $dbh;

    protected function __construct() {
        try {
            $this->dbh = new \PDO('mysql:host=' . SETTINGS['DB_HOST'] . ';dbname=' . SETTINGS['DB_DATABASE'], SETTINGS['DB_USER'], SETTINGS['DB_PASS']);
            $this->install = SETTINGS['INSTALL'];
        } catch (\PDOException $e) {
            echo 'Cannot connect to database.';
            exit;
        }
    }

    protected function getDataForModel(string $query, string $model): array {
        $data = [];
        try {
            foreach ($this->dbh->query($query) as $item) {
                $tmpData = new $model($item);
                array_push($data, $tmpData);
            }

            return $data;
        } catch (\PDOException $e) {
            echo 'cannot fetch data from database';
            exit;
        }
    }

}
