<?php

namespace IssueList\Models;

class Transition {

    public $from;
    public $to;

    function __construct(array $array) {
        $this->from = $array['from_state'];
        $this->to = $array['to_state'];
    }

}
