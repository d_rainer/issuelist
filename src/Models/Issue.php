<?php

namespace IssueList\Models;

class Issue {

    public $id;
    public $name;
    public $stateId;

    function __construct(array $array) {
        $this->id = $array['id'];
        $this->name = $array['title'];
        $this->stateId = $array['state'];
    }

}
