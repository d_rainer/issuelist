<?php

namespace IssueList\Models;

class State {

    public $id;
    public $name;
    public $priority;

    function __construct(array $array) {
        $this->id = $array['id'];
        $this->name = $array['name'];
        $this->priority = $array['priority'];
    }

}
