<?php

require "vendor/autoload.php";

use PHPUnit\Framework\TestCase,
    IssueList\Controller\TransitionController,
    IssueList\Controller\IssueController,
    IssueList\Controller\StateController;

class IssueListTest extends TestCase {

    public function setUp() {
        $this->stateController = new StateController();
        $this->transitionController = new TransitionController();
        $this->issueController = new IssueController();
    }

    /*
     * check if there is no state, which isn't the start or the end of a transition
     * (assume, that no isolated state is allowed)
     * check if each state has a name and a priority >= 0
     */

    public function testStatesConsistency() {
        $states = $this->stateController->getStates();
        $transitions = $this->transitionController->getTransitions();

        $transitionIds = [];
        foreach ($transitions as $transition) {
            array_push($transitionIds, $transition->from);
            array_push($transitionIds, $transition->to);
        }
        array_unique($transitionIds); // array of state_id's, which are the start or the end of a transition

        foreach ($states as $state) {
            $this->assertContains($state->id, $transitionIds, 'state_id ' . $state->id . ' not found in the transitions table');
            $this->assertNotEmpty($state->name, 'state with id=' . $state->id . ' has no name / an empty name');
            $this->assertTrue($state->priority >= 0, 'priority of a state has to be greater or equal zero');
        }
    }

    /**
     * check if there is no incorrupt data in the database
     * this means e.g. an issue which has an incorrect state (but should be covered by the foreign key constraints anyway)...
     */
    public function testIssuesConsistency() {
        $states = $this->stateController->getStates();
        $issues = $this->issueController->getIssues();

        $stateIds = [];
        foreach ($states as $state) {
            array_push($stateIds, $state->id);
        }

        foreach ($issues as $issue) {
            $this->assertContains($issue->stateId, $stateIds, 'issue with id=' . $issue->id . ' has an incorrect value for the state');
            $this->assertNotEmpty($issue->name, 'issue with id=' . $issue->id . ' has no title / an empty title');
        }
    }

    /**
     * check if the transitions are well-defined
     * e.g. start and end of a transition is a state which is also saved in the database
     * (should be covered by the foreign key constraint anyway)
     */
    public function testTransitionsConsistency() {
        $transitions = $this->transitionController->getTransitions();
        $states = $this->stateController->getStates();

        $stateIds = [];
        foreach ($states as $state) {
            array_push($stateIds, $state->id);
        }

        foreach ($transitions as $transition) {
            $this->assertContains($transition->from, $stateIds);
            $this->assertContains($transition->to, $stateIds);
        }
    }

    /**
     * check the number of issues
     */
    public function testIssueCount() {
        $issues = $this->issueController->getIssues();
        $this->assertTrue(count($issues) >= 7, 'as the data is given in this example, the count of the issues has to be greater or equal 7');
    }

    /**
     * check the number of states
     */
    public function testStateCount() {
        $states = $this->stateController->getStates();
        $this->assertTrue(count($states) >= 5, 'as the data is given in this example, the count of the states has to be greater or equal 5');
    }

    /**
     * check the number of transitions
     */
    public function testTransitionCount() {
        $transitions = $this->transitionController->getTransitions();
        $this->assertTrue(count($transitions) >= 6, 'as the data is given in this example, the count of the states has to be greater or equal 6');
    }

}
