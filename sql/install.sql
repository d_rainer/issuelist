CREATE TABLE IF NOT EXISTS states (
    id int NOT NULL AUTO_INCREMENT,
    name VARCHAR(30),
    priority DECIMAL(3,1) DEFAULT 0,
    PRIMARY KEY (id)
);


CREATE TABLE IF NOT EXISTS issues (
    id int NOT NULL AUTO_INCREMENT,
    title VARCHAR(50),
    state int NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (state) REFERENCES states(id)
);


CREATE TABLE IF NOT EXISTS state_transitions (
    from_state int NOT NULL,
    to_state int NOT NULL,
    FOREIGN KEY (from_state) REFERENCES states(id),
    FOREIGN KEY (to_state) REFERENCES states(id)
);

SET foreign_key_checks = 0;
TRUNCATE states;
TRUNCATE issues;
TRUNCATE state_transitions;
SET foreign_key_checks = 1;

INSERT INTO `states` (`name`, `priority`) VALUES ('to do', 1), ('doing', 3), ('on hold', 2), ('done', 4), ('failed', 5);
INSERT INTO `state_transitions` (`from_state`, `to_state`) VALUES (1,2), (1,3), (2,3), (2,4), (2,5), (3,2);


INSERT INTO `issues` (`title`, `state`) VALUES
('Get new coffee machine', 4),
('(Re)Fill Beans', 2),
('Fill water tank', 1),
('Make coffee', 3),
('Make more coffee', 1),
('Turn old coffee machine off and on again', 5),
('Repair old coffee machine', 5);