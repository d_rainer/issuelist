<?php

require "vendor/autoload.php";

use IssueList\Controller\SortController;

$sortController = new SortController();

echo '--------------------------------------------------------' . "\n";
echo 'Sort states and list issues under their respective state:' . "\n";
echo '--------------------------------------------------------' . "\n";
$sortController->listIssuesByState();
echo "\n\n";


echo '--------------------------------------------------------' . "\n";
echo 'Add some new issues to the list:' . "\n";
echo '--------------------------------------------------------' . "\n";
$sortController->addIssuesAndListIssues();


echo '--------------------------------------------------------' . "\n";
echo 'Add a new state "Try again" after "Failed":' . "\n";
echo '--------------------------------------------------------' . "\n";

$sortController->addNewStateAndListIssues();
